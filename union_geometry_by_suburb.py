import fiona
import pandas as pd
import geopandas as gpd
from shapely.geometry.geo import shape
from shapely.ops import cascaded_union
from matplotlib import pyplot as plt
from shapely.geometry import box
import pyproj as ppj


def union_geometry_by_suburb():
    try:
        print("Pandas version: " + pd.__version__)
        print("Geopandas version: " + gpd.__version__)
        print("pyproj version: " + ppj.__version__)
        fgdb_path = r'DFGTOut2.gdb'
        layers = fiona.listlayers(fgdb_path)
        gdf_all = None
        counter = 0
        for layer in layers:
            fc = fiona.open(fgdb_path,layer=layer)
            for feature in fc:
                if 'relationship' not in feature['properties']:
                    continue
                if 'suburboid' not in feature['properties']:
                    continue            
                if feature['properties']['relationship'] == 'WITHIN' and feature['properties']['suburboid'] == 1:
                    #polygons.append(feature['geometry'])
                    #feature['_geom'] = feature['geometry']
                    #polygons.append(feature)
                    #print(str(feature))
                    df = pd.DataFrame()
                    df['geo_shape'] = shape(feature['geometry'])
                    if counter == 0:
                        gdf_all = gpd.GeoDataFrame(df, geometry='geo_shape', crs="EPSG:28356")
                        counter = counter + 1
                    else:
                        gdf = gpd.GeoDataFrame(df, geometry='geo_shape', crs="EPSG:28356")
                        gdf_all = gdf_all.append(gdf)
                        counter = counter + 1

        union = gdf_all.geometry.unary_union
        gdf_out = gpd.GeoDataFrame(geometry=[union])
        gdf_out.plot()
        plt.show()

        gdf_out.to_file('union_out.shp')

        # Next line throws exception: unsupported mode: 'w'
        # gdf_out.to_file(filename='union_out.gdb', driver='OpenFileGDB', layer="union_layer")

    except Exception as e:
        print (str(e))


if __name__ == '__main__':
    try:
        union_geometry_by_suburb()
    except Exception as e:
        print("An exception occurred: " + str(e))